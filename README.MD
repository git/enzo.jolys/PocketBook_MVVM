# Projet MVVM - Enzo Jolys

## Lancez le Projet

    Lancez avec la solution -> MVVM/MVVM.sln 
        - Nettoyer la solution
        - Générer la solution 

## Les documents important 

### Les Pages 

    Les pages de l'application sont dans -> MVVM/MVVM/Pages 

### Les VM applicatif 

    Les VM applicatif de l'application sont dans -> MVVM/MVVM/ViewModel

### Les VM Wrapper

    Les VM Wrapper de l'application sont dans -> MVVM/Wrapper

### Les différentes classe 

    Les différentes classe sont dans -> MVVM/Model

### Route de l'application 

    Les routes de l'aplication sont définie dans une classe static dans -> MVVM/MVVM/Constante

## Bugs/Cas d'erreur connue 

### Page MyLibraryPage

    - Les nombres de livres ne se mette pas à jour lors de l'ajout d'un livre à lire plus tard 

### Page Books 

    - Comme certain livre ont plusieurs autheur, ils vont être affiché plusieur fois (C'est voulu)
    - Le binding de Statut de ne fonctionne pas 

### Page Books + Book 

    La liste des auteurs de s'affiche pas dans mes composents 


## Dernier commit

    Le commit avec le tag v1.0 - "Rendu de tp MVVM"

## Platforme 

    Mon projet à été teste sur un android. 

