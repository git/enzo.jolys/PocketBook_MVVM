﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Classe
{
    public class Date
    {
        public string date {  get; set; }
        public string nbLivre { get; set; }

        public Date(string date, string nbLivre)
        {
            this.date = date;
            this.nbLivre = nbLivre;
        }
    }
}
