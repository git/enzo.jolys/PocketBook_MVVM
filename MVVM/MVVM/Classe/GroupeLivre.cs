﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Classe
{
    public class GroupeLivre : List<Livre>
    {
        public string Auteur { get; set; }

        public GroupeLivre( string auteur,List<Livre> listLivre) : base(listLivre)
        {
            Auteur = auteur;
        }
    }
}
