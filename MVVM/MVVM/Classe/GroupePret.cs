﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Classe
{
    public class GroupePret : List<Livre>
    {
        public string name { get; set; }

        public GroupePret(string name, List<Livre> listLivre) : base(listLivre)
        {
            this.name = name;
        }
    }
}
