﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Classe
{
    public class Auteur
    {
        public string Name { get; set; }
        public string NbLivre { get; set; }
        public Auteur(string name, string nbLivre)
        {
            Name = name;
            NbLivre = nbLivre;
        }
    }
}
