﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Classe
{
    public class Livre
    {
        public string Image { get; set;  }

        public string Titre { get; set; }

        public string Auteur { get; set; }

        public string Note { get; set; }

        public string Statut { get; set; }

        public bool Separateur { get; set; }

        public Livre()
        {
            Separateur = true;
        }
    }
}
