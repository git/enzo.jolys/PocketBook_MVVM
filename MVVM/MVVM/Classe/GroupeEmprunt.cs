﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Classe
{
    public class GroupeEmprunt : List<Livre>
    {
        public string name {  get; set; }

        public GroupeEmprunt(string name, List<Livre> listLivre) : base(listLivre)
        {
           this.name = name;
        }
    }
}
