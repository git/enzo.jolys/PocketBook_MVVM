﻿using MVVM.Classe;

namespace MVVM.Pages;

public partial class EmpruntPret : ContentPage
{
	public List<GroupeEmprunt> groupeEmprunts { get; set; } = new List<GroupeEmprunt>();
	public List<GroupePret> groupePret { get; set; } = new List<GroupePret> { };

	public EmpruntPret()
	{
		InitializeComponent();
		getEmprunt();
		getPret();

		collectionView.ItemsSource = groupePret;
	}

	public void onClickPret(object sender, EventArgs args)
	{
		collectionView.ItemsSource = groupePret;
		empruntButton.BackgroundColor = Color.FromRgba(0, 0, 0, 0);
		pretButton.BackgroundColor = Color.FromArgb("#FFFFFFFF");

	}

	public void onClickEmprunt(object sender, EventArgs args)
	{
		collectionView.ItemsSource = groupeEmprunts;
		pretButton.BackgroundColor = Color.FromRgba(0, 0, 0, 0);
		empruntButton.BackgroundColor = Color.FromArgb("#FFFFFFFF");
	}

	private void getEmprunt()
	{
		groupeEmprunts.Add(new GroupeEmprunt("Antoine",
			new List<Livre> {
							new Livre { Auteur = "Eiichirō Oda",
										Titre = "One piece tome 1",
										Note = "5",
										Statut = "Lu",
										Image = "onepiece1"}}));

		groupeEmprunts.Add(new GroupeEmprunt("Pierre",
			new List<Livre>
			{
				new Livre { Auteur = "Masashi Kishimoto",
										Titre = "Naruto tome 1",
										Note = "4",
										Statut = "Lu",
										Image = "naruto1"}}));
	}


	private void getPret()
	{
        groupePret.Add(new GroupePret("Tim",
            new List<Livre> {
							new Livre { Auteur = "Tite Kubo",
										Titre = "Bleach tome 1",
										Note = "3",
										Statut = "Lu",
										Image = "bleach1"},
							new Livre { Auteur = "Masashi Kishimoto",
                                        Titre = "Naruto tome 2",
                                        Note = "2",
                                        Statut = "Lu",
                                        Image = "naruto2"}}));

        groupePret.Add(new GroupePret("Stéphanie",
            new List<Livre> {
							new Livre { Auteur = "Tite Kubo",
										Titre = "Bleach tome 1",
										Note = "3",
										Statut = "Lu",
										Image = "bleach1"}}));

    }
}