using Microsoft.Maui.Controls;
using MVVM.ViewModel.Books;
using Wrapper;

namespace MVVM.Pages;

public partial class FilterPage : ContentPage
{
	public FilterBookViewModel FilterBookViewModel { get; set; }

	public BooksNavigateAndLoad BooksNavigateAndLoad { get; set; }

	public FilterPage(FilterBookViewModel filterBookViewModel,BooksNavigateAndLoad booksNavigateAndLoad)
	{

		this.FilterBookViewModel = filterBookViewModel;
		this.BooksNavigateAndLoad = booksNavigateAndLoad;

		BindingContext = this;

		InitializeComponent();
        collectionView.ItemsSource = FilterBookViewModel.ItemFilters;

    }
}