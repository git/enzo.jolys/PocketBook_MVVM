namespace MVVM.Pages.Component;

public partial class FilterItemComponent : ContentView
{

    public static readonly BindableProperty DataFilterCompProperty = BindableProperty.Create(nameof(DataFilterComp), typeof(string), typeof(FilterItemComponent), string.Empty);
    public static readonly BindableProperty NumberBooksCompProperty = BindableProperty.Create(nameof(NumberBooksComp), typeof(string), typeof(FilterItemComponent), string.Empty);


    public string DataFilterComp
    {
        get => (string)GetValue(FilterItemComponent.DataFilterCompProperty);
        set
        {
            Console.WriteLine(value);
            SetValue(FilterItemComponent.DataFilterCompProperty, value);
        }
    }
    public string NumberBooksComp
    {
        get => (string)GetValue(FilterItemComponent.NumberBooksCompProperty);
        set => SetValue(FilterItemComponent.NumberBooksCompProperty, value);
    }


    public FilterItemComponent()
	{
		InitializeComponent();
        DataFilterXAML.SetBinding(Label.TextProperty, new Binding(nameof(DataFilterComp), source: this));
        NumberBooksXAML.SetBinding(Label.TextProperty, new Binding(nameof(NumberBooksComp), source: this));
    }
}