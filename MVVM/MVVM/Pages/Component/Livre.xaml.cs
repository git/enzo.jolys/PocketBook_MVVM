
using Model;
using System.Reflection;
using ToolKit;

namespace MVVM.Pages.Component;

public partial class Livre : ContentView
{
    
    public static readonly BindableProperty TitreCompProperty = BindableProperty.Create(nameof(TitreComp), typeof(string), typeof(Livre), string.Empty);
    public static readonly BindableProperty ImageCompProperty = BindableProperty.Create(nameof(ImageComp), typeof(string), typeof(Livre), string.Empty);
    public static readonly BindableProperty AuteurCompProperty = BindableProperty.Create(nameof(AuteurComp), typeof(string), typeof(string),null);
    public static readonly BindableProperty UserRatingCompProperty = BindableProperty.Create(nameof(UserRatingComp), typeof(float), typeof(Livre), null);
    //public static readonly BindableProperty StatutCompProperty = BindableProperty.Create(nameof(StatusComp),typeof(Status), typeof(Livre), null);

    /*
    public Status StatusComp
    {
        get => (Status)GetValue(Livre.StatutCompProperty);
        set => SetValue(Livre.StatutCompProperty, value);
    }*/


    public string TitreComp
    {
        get => (string)GetValue(Livre.TitreCompProperty);
        set => SetValue(Livre.TitreCompProperty, value);
    }

    public string ImageComp
    {
        get => (string)GetValue(Livre.ImageCompProperty);
        set => SetValue(Livre.ImageCompProperty, value);
    }

    public string AuteurComp
    {
        get
        {   
            /*
            List<Author> list = (List<Author>)GetValue(Livre.AuteurCompProperty);
            string auth = "";
            for(int i = 0 ; i < list.Count;i++)
            {
                auth += list[i].Name;
                if ( i < list.Count - 1)
                {
                    auth += ", ";
                }
            }
            return auth;*/
            return (string)GetValue(Livre.AuteurCompProperty);
        }
        set => SetValue(Livre.AuteurCompProperty, value);
    }

    public float UserRatingComp
    {
        get
        {
            float note = (float)GetValue(Livre.UserRatingCompProperty);
            imageStar(note);
            return note;
        }
        set => SetValue(Livre.UserRatingCompProperty, value);       
    }


    public Livre()
	{
		InitializeComponent();
        TitreXAML.SetBinding(Label.TextProperty, new Binding(nameof(TitreComp), source: this));
        ImageXAML.SetBinding(Image.SourceProperty, new Binding(nameof(ImageComp), source: this));
        AuteurXAML.SetBinding(Label.TextProperty, new Binding(nameof(AuteurComp), source: this));
        NoteXAML.SetBinding(Label.TextProperty, new Binding(nameof (UserRatingComp), source: this));
        //StatutXAML.SetBinding (Label.TextProperty, new Binding(nameof(StatusComp), source: this));
    }

    
    private void imageStar(float note) 
    {   
        int tmp = (int)note;

        if (tmp > 0 )
        {
            etoile1.Source = "etoile";
        }
        if (tmp > 1)
        {
            etoile2.Source = "etoile";
        }
        if (tmp > 2)
        {
            etoile3.Source = "etoile";
        }
        if (tmp > 3)
        {
            etoile4.Source = "etoile";
        }
        if (tmp == 5)
        {
            etoile5.Source = "etoile";
        }
    }
    
    /*
    public string ImageSmall
    {
        get { return imageSmall; }
        set { imageSmall = value; }
    }
    private string imageSmall;

    public string Title
    {
        get { return title; }
        set { title = value; }
    }
    private string title;

    public string Author
    {
        get { return author; }
        set { author = value; }
    }
    private string author;

    public string Note
    {
        get { return note; }
        set { note = value; }
    }
    private string note;
    

    public Livre()
    {
        InitializeComponent();      
        //ImageXAML.SetBinding(Image.SourceProperty, ImageSmall);
        //AuteurXAML.SetBinding(Label.TextProperty, Author);
        //NoteXAML.SetBinding (Label.TextProperty, Note);
    }*/

}