namespace MVVM.Pages;
using Microsoft.Maui.Controls;
using Model;
using System.Windows.Input;
using Wrapper;

public partial class BookPage : ContentPage
{
	public BookViewModel BookViewModel { get; set; }

	public ICommand AddBookToBeRead { get; set; }


	public BookPage(BookViewModel bookViewModel)
	{
		this.BookViewModel = bookViewModel;
        AddBookToBeRead = new Command(AddBookToBeReadFonction);
        BindingContext = this;
        
        InitializeComponent();
        TexteToBeRead.TextColor = Color.FromRgb(255, 0, 0);
        IconToBeRead.TintColor = Color.FromRgb(255, 0, 0);
    }

	public void AddBookToBeReadFonction()
	{
        TexteToBeRead.TextColor = Color.FromRgb(0,255,0);
		IconToBeRead.TintColor = Color.FromRgb(0,255,0);
		BookViewModel.AddBookToBeRead.Execute();
    }
}