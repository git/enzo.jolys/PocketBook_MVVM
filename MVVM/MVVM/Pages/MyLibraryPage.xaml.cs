using MVVM.ViewModel;
using MVVM.ViewModel.Books;
using Wrapper;

namespace MVVM.Pages;

public partial class MyLibraryPage : ContentPage
{
	public BooksNavigateAndLoad BooksNavigateAndLoad { get; set;}

	public MyLibraryViewModel MyLibraryViewModel { get; set;}

	public MyLibraryPage(BooksNavigateAndLoad booksNavigateAndLoad,MyLibraryViewModel myLibraryViewModel)
	{
		this.BooksNavigateAndLoad = booksNavigateAndLoad;
		this.MyLibraryViewModel = myLibraryViewModel;
		BindingContext = this;

		// Charg� les donn�es 
		MyLibraryViewModel.LoadData.Execute();

        InitializeComponent();
    }

}