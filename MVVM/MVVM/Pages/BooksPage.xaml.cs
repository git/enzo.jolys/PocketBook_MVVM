﻿
using Model;
using MVVM.ViewModel;
using System.Windows.Input;
using Wrapper;

namespace MVVM.Pages;

public partial class BooksPage : ContentPage
{

    // Pour récupérer les données 
    public BooksViewModel BooksViewModel { get; set; }

    // Permet de faire la navigation pour la page BookPage
    public BookCheckAndNavigate BookCheckAndNavigate { get; set; }


    public BooksPage(BooksViewModel bookViewModel, BookCheckAndNavigate checkAndNavigate)
    {
        this.BooksViewModel = bookViewModel;      
        this.BookCheckAndNavigate = checkAndNavigate;

        BindingContext = this;

        InitializeComponent();
        collectionView.ItemsSource = BooksViewModel.GroupBooks;
    }

}