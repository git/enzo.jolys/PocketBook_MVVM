﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.Constante
{
    public static class GlobalConst
    {

        public const string MyLibraryPage = "//MyLibraryPage";
        public const string BooksPage = "//BooksPage";
        public const string BookPage = "//BooksPage/BookPage";
        public const string FilterPage = "//MyLibraryPage/FilterPage";
    }
}
