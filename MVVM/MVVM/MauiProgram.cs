﻿using CommunityToolkit.Maui;
using Microsoft.Extensions.Logging;
using Model;
using MVVM.Pages;
using MVVM.ViewModel;
using MVVM.ViewModel.Books;
using StubLib;
using ToolKit;
using Wrapper;

namespace MVVM
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();

            builder.Services.AddSingleton<ILibraryManager, LibraryStub>()
                            .AddSingleton<IUserLibraryManager, UserLibraryStub>()

                            //VM Applicatif
                            .AddSingleton<NavigationViewModel>()
                            .AddSingleton<BooksNavigateAndLoad>()
                            .AddSingleton<MyLibraryLoadData>()
                            .AddSingleton<BookCheckAndNavigate>()
                            .AddSingleton<DataViewModel>()

                            //Wrapper
                            .AddSingleton<BooksViewModel>()
                            .AddSingleton<MyLibraryViewModel>()
                            .AddSingleton<BookViewModel>()
                            .AddSingleton<FilterBookViewModel>()

                            // Page
                            .AddScoped<BookPage>()
                            .AddSingleton<BooksPage>()
                            .AddSingleton<MyLibraryPage>()
                            .AddSingleton<FilterPage>()

                            // Toolkit 
                            .AddSingleton<WorkOnListBooks>();



            builder
                .UseMauiApp<App>()
                .UseMauiCommunityToolkit()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
		builder.Logging.AddDebug();
#endif

            return builder.Build();
        }
    }
}