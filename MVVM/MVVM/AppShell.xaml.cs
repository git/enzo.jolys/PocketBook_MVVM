﻿using MVVM.Constante;
using MVVM.Pages;

namespace MVVM
{
    public partial class AppShell : Shell
    {
        
        public AppShell()
        {
            Routing.RegisterRoute(GlobalConst.MyLibraryPage, typeof(MyLibraryPage));
            Routing.RegisterRoute(GlobalConst.BooksPage, typeof(BooksPage));
            Routing.RegisterRoute(GlobalConst.BookPage, typeof(BookPage));
            Routing.RegisterRoute(GlobalConst.FilterPage, typeof(FilterPage));

            InitializeComponent();
        }
    }
}