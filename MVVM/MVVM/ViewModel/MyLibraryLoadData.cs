﻿using MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Wrapper;

namespace MVVM.ViewModel
{
    public class MyLibraryLoadData
    {
        public MyLibraryViewModel  LibraryViewModel { get; set; }

        public ICommand CommandNavigationAndLoad { set; get; }

        public MyLibraryLoadData(NavigationViewModel navigateViewModel, MyLibraryViewModel libraryViewModel)
        {
            this.LibraryViewModel = libraryViewModel;

            CommandNavigationAndLoad = new Command<string>(NavigateAndLoadFonction);
        }

        private void NavigateAndLoadFonction(string arg)
        {
            LibraryViewModel.LoadData.Execute();           
        }
    }
}
