﻿using Model;
using MVVM.Constante;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToolKit;
using Wrapper;

namespace MVVM.ViewModel.Books
{
    public class BooksNavigateAndLoad
    {
        // Navigation
        public NavigationViewModel NavigateViewModel { get; set; }

        // Wrapper


        //VM
        public DataViewModel DataViewModel { get; set; }


        // Command 
        public ICommand CommandNavigationAndLoadBooks { set; get; }
        public ICommand CommandNavigationFilterAndLoadData { set; get; }

        public ICommand CommandFilterPageToBooksPage { set; get; }


        public BooksNavigateAndLoad(NavigationViewModel navigateViewModel,DataViewModel dataViewModel)
        {
            NavigateViewModel = navigateViewModel;
            DataViewModel = dataViewModel;

            CommandNavigationAndLoadBooks = new Command<ICommand>(NavigateAndLoadBooks);
            CommandNavigationFilterAndLoadData = new Command<ICommand>(NavigateFilterPageAndLoadData);
            CommandFilterPageToBooksPage = new Command<Tuple<TypeItemFilter,string>>(FilterPageToBooksPage);
        }


        private void NavigateAndLoadBooks(ICommand command)
        {
            command.Execute(null);

            NavigateViewModel.CommandNavigation.Execute(GlobalConst.BooksPage);
        }

        private void NavigateFilterPageAndLoadData(ICommand command)
        {
            command.Execute(null);

            NavigateViewModel.CommandNavigation.Execute(GlobalConst.FilterPage);
        }


        private void FilterPageToBooksPage(Tuple<TypeItemFilter,string> tuple)
        {
            switch ( tuple.Item1)
            {
                case TypeItemFilter.Author:
                    DataViewModel.FilterAuthorToBooks.Execute(tuple.Item2);
                    break;

                case TypeItemFilter.PublishDate:
                    DataViewModel.FilterPublishDateToBooks.Execute(tuple.Item2);
                    break;

                case TypeItemFilter.Note:
                    DataViewModel.FilterNoteToBooks.Execute(tuple.Item2);
                    break;
            }
            NavigateViewModel.CommandNavigation.Execute(GlobalConst.BooksPage);

        }

    }
}
