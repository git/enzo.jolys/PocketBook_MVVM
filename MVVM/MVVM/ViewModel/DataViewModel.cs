﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Wrapper;

namespace MVVM.ViewModel
{
    public class DataViewModel
    {
        // Liste des Wrappers
        private BooksViewModel BooksViewModel { get; set; }
        private FilterBookViewModel FilterBookViewModel { get; set; }


        // Liste des commands
        public ICommand BooksAllDataCommand { get; set; }
        public ICommand BooksLoanDataCommand { get; set; }
        public ICommand BooksToBeReadDataCommand { get; set; }
        public ICommand BooksFavoriteDataCommand { get; set; }
        public ICommand FilterAuthorDataCommand { get; set; }
        public ICommand FilterPublishDateDataCommand { get; set; }
        public ICommand FilterAuthorToBooks {  get; set; }
        public ICommand FilterPublishDateToBooks { get; set; }
        public ICommand FilterNoteDataCommand { get; set; }
        public ICommand FilterNoteToBooks { get; set; }

        public DataViewModel(BooksViewModel booksViewModel,FilterBookViewModel filterBookViewModel)
        {
            this.BooksViewModel = booksViewModel;
            this.FilterBookViewModel = filterBookViewModel;

            BooksAllDataCommand = new Command(BooksAll);
            BooksLoanDataCommand = new Command(BooksLoan);
            BooksToBeReadDataCommand = new Command(BooksToBeRead);
            BooksFavoriteDataCommand = new Command(BooksFavorite);
            FilterAuthorDataCommand = new Command(FilterAuthor);
            FilterPublishDateDataCommand = new Command(FilterPublishDate);
            FilterAuthorToBooks = new Command<string>(BooksByAuthor);
            FilterPublishDateToBooks = new Command<string>(BooksByYear);

            FilterNoteToBooks = new Command<string>(BooksByNote);

            FilterNoteDataCommand = new Command(FilterNote);
        }


        private void BooksAll()
        {
            BooksViewModel.LoadBooks.Execute();
        }

        private void BooksLoan()
        {
            BooksViewModel.LoadBooksLoan.Execute();
        }

        private void BooksToBeRead()
        {
            BooksViewModel.LoadBooksToBeRead.Execute();
        }

        private void BooksFavorite()
        {
            BooksViewModel.LoadBooksFavorite.Execute();
        }

        private void BooksByAuthor(string author)
        {
            BooksViewModel.LoadBooksByAuthor.Execute(author);
        }

        private void BooksByYear(string year)
        {
            BooksViewModel.LoadBooksByYear.Execute(year);
        }

        private void BooksByNote(string note)
        {
            BooksViewModel.LoadBooksByNote.Execute(note);
        }

        //------------------------------------//


        private void FilterAuthor()
        {
            FilterBookViewModel.LoadDataAuthor.Execute();
        }

        private void FilterPublishDate()
        {
            FilterBookViewModel.LoadDataPublishDate.Execute();
        }

        private void FilterNote()
        {
            FilterBookViewModel.LoadDataNote.Execute();
        }
    }
}
