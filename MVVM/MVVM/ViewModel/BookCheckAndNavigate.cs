﻿using MVVM.Constante;
using MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Wrapper;

namespace MVVM.ViewModel
{
    public class BookCheckAndNavigate
    {
        public ICommand CommandBookCheckAndNavigate { get; set; }

        NavigationViewModel NavigationViewModel { get; set; }
        BookViewModel BookViewModel { get; set; }

        public BookCheckAndNavigate(NavigationViewModel navigateViewModel,BookViewModel bookViewModel)
        {
            this.NavigationViewModel = navigateViewModel;
            this.BookViewModel = bookViewModel;

            CommandBookCheckAndNavigate = new Command<string>(CheckAndNavigate);
        }

        private void CheckAndNavigate(string isbn13)
        {
            if ( BookViewModel.LoadBook(isbn13))
            {
                NavigationViewModel.CommandNavigation.Execute(GlobalConst.BookPage);
            }
        }
    }
}
