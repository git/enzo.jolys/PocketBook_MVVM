﻿using MVVM.Constante;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVM.ViewModel
{
    public class NavigationViewModel 
    {
        public ICommand CommandNavigation { set; get; }
       

        public NavigationViewModel()
        {
            
            CommandNavigation = new Command<string>(Navigate);
        }

        private async void Navigate(string arg)
        {
            await Shell.Current.GoToAsync(arg);
        }
    }
}
