﻿using Model;
using ToolKit;

namespace Wrapper
{
    public class MyLibraryViewModel
    {
        public RelayCommand<string> LoadData { get; private set; }
        IUserLibraryManager UserLibraryManager { get; set; }
         

        public MyLibraryViewModel(IUserLibraryManager userLibraryManager) 
        {
            this.UserLibraryManager = userLibraryManager;

            LoadData = new RelayCommand<string>(o => LoadDataFonction());
        }

        private void LoadDataFonction()
        {

            var result = UserLibraryManager.GetBooksFromCollection(0,(int)UserLibraryManager.GetBooksFromCollection(0,0).Result.Item1);

            // Books
            long nb = result.Result.Item1;
            if (nb == 0 )
            {
                nbBooks = " ";
            }
            else
            {
                nbBooks = nb.ToString();
            }

            // Loan
            nb = UserLibraryManager.GetPastLoans(0, 0).Result.Item1;
            if (nb == 0)
            {
                nbLoans = " ";
            }
            else
            {
                nbLoans = nb.ToString();
            }

            // ToBeRead
            int compteur = 0; 
            foreach(Book book in result.Result.Item2)
            {
                if ( book.Status == Status.ToBeRead )
                {
                    compteur++;
                }
            }
            if (  compteur == 0 )
            {
                nbToBeRead = " ";
            }
            else
            {
                nbToBeRead = compteur.ToString();
            }

            // Favorite
            nbFavorite = UserLibraryManager.GetFavoritesBooks(0, 0).Result.Item1.ToString();
        }


        public string NbBooks
        {
            get => nbBooks;
            set {
                nbBooks = value;
            }
        }
        private string nbBooks;

        public string NbLoans
        {
            get => nbLoans;
            set {
                nbLoans = value;
            }
        }
        private string nbLoans;

        public string NbToBeRead
        {
            get => nbToBeRead;
            set {
                nbToBeRead = value;
            }
        }
        private string nbToBeRead;

        public string NbFavorite
        {
            get => nbFavorite;
            set {
                nbFavorite = value;
            }
        }
        private string nbFavorite;
    }
}

