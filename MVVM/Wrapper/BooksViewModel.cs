﻿using ToolKit;
using Model;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Data.Common;

namespace Wrapper
{
    public class BooksViewModel : BaseViewModel
    {
        IUserLibraryManager UserLibraryManager { get; set; }

        public WorkOnListBooks WorkOnListBooks { get; set; } 

        // Commandes
        public RelayCommand<string> LoadBooks { get; private set; }
        public RelayCommand<string> LoadBooksLoan { get; private set; }
        public RelayCommand<string> LoadBooksToBeRead { get; private set; }
        public RelayCommand<string> LoadBooksFavorite { get; private set; }
        public RelayCommand<string> LoadBooksByAuthor { get; private set; }
        public RelayCommand<string> LoadBooksByYear { get; private set; }
        public RelayCommand<string> LoadBooksByNote { get; private set; }


        // Liste de books 
        public ObservableCollection<Book> Books = new ObservableCollection<Book>();
        public ObservableCollection<GroupBooks> GroupBooks { get; private set; } = new ObservableCollection<GroupBooks>();

        // ------------------------------------------------------------------------------//
        public int Index
        {
            get { return index; }
            private set
            {
                setProperty(ref index, value);
            }

        }
        private int index;



        public int Count
        {
            get { return count; }
            private set
            {
                setProperty(ref count, value);
            }

        }
        private int count;

        // ------------------------------------------------------------------------------//

        public BooksViewModel(IUserLibraryManager userLibraryManager,WorkOnListBooks workOnListBooks)
        {
            this.UserLibraryManager = userLibraryManager;
            this.WorkOnListBooks = workOnListBooks;


            LoadBooks = new RelayCommand<string>(o => LoadBooksFonction());
            LoadBooksLoan = new RelayCommand<string>(o => LoadBooksLoanFonction());
            LoadBooksToBeRead = new RelayCommand<string>(o => LoadBooksToBeReadFonction());
            LoadBooksFavorite = new RelayCommand<string>(o => LoadBooksFavoriteFonction());
            LoadBooksByAuthor = new RelayCommand<string>(o => LoadBooksByAuthorFonction(o));
            LoadBooksByYear = new RelayCommand<string>(o => LoadBooksByYearFonction(o));
            LoadBooksByNote = new RelayCommand<string>(o => LoadBooksByNoteFonction(o));

            InitiateParam();
        }

        private void InitiateParam()
        {
            Index = 0;
            Count = 6;
        }


        // All books
        private void LoadBooksFonction()
        {
            var result = UserLibraryManager.GetBooksFromCollection(index,count,"");
            if (result.Result.Item2.Count() == 0 ) { return ; }
            Books.Clear();
            foreach (Book book in result.Result.Item2)
            {
                Books.Add(book);
            }

            List<GroupBooks> groupeBook =  WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (groupeBook.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in groupeBook)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
           
        }

        // Loan Books
        private void LoadBooksLoanFonction()
        {
            var result = UserLibraryManager.GetPastLoans(index, count);
            if (result.Result.Item2.Count() == 0) { return; }
            Books.Clear();
            foreach (Loan loan in result.Result.Item2)
            {
                Books.Add(loan.Book);
            }

            List<GroupBooks> loanGroupBooks = WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (loanGroupBooks.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in loanGroupBooks)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
        }

        // ToBeRead Books
        private void LoadBooksToBeReadFonction()
        {
            var result = UserLibraryManager.GetBooksFromCollection(0,((int)UserLibraryManager.GetBooksFromCollection(0,0).Result.Item1), "");

            if (result.Result.Item2.Count() == 0) { return; }
            Books.Clear();
            GroupBooks.Clear();

            foreach (Book book in result.Result.Item2)
            {
                Console.WriteLine(book.Title);
                Console.WriteLine(book.Status.ToString());

                if (book.Status == Status.ToBeRead)
                {
                    Books.Add(book);
                }
            }

            List<GroupBooks> groupeBook = WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (groupeBook.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in groupeBook)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
        }



        // Favorite Books
        private void LoadBooksFavoriteFonction()
        {
            var result = UserLibraryManager.GetFavoritesBooks(index,((int)UserLibraryManager.GetBooksFromCollection(0, 0).Result.Item1));
            if (result.Result.Item2.Count() == 0) { return; }
            Books.Clear();
            foreach (Book book in result.Result.Item2)
            {
                Books.Add(book);
            }

            List<GroupBooks> favoriteGroupBook = WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (favoriteGroupBook.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in favoriteGroupBook)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
        }

        // Books by author
        private void LoadBooksByAuthorFonction(string author)
        {
            List<Book> result = UserLibraryManager.GetBooksByAuthorName(author).ToList();

            if (result.Count == 0) { return; }
            Books.Clear();

            foreach (Book book in result)
            {
                Books.Add(book);
            }

            List<GroupBooks> GroupBook = WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (GroupBook.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in GroupBook)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
        }

        // Books by Year
        private void LoadBooksByYearFonction(string year)
        {
            List<Book> result = UserLibraryManager.GetBooksByYear(year).ToList();

            if (result.Count == 0) { return; }
            Books.Clear();

            foreach (Book book in result)
            {
                Books.Add(book);
            }

            List<GroupBooks> GroupBook = WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (GroupBook.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in GroupBook)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
        }

        private void LoadBooksByNoteFonction(string note)
        {
            
            List<Book> result = UserLibraryManager.GetBooksByNote(note).ToList();

            if (result.Count == 0) { return; }
            Books.Clear();

            foreach (Book book in result)
            {
                Books.Add(book);
            }

            List<GroupBooks> GroupBook = WorkOnListBooks.UpdateGroupBooks(Books.ToList());
            if (GroupBook.Count > 0)
            {
                GroupBooks.Clear();
                foreach (GroupBooks groupeBookUnique in GroupBook)
                {
                    GroupBooks.Add(groupeBookUnique);
                }
            }
        }
    }
}