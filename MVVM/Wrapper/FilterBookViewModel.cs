﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolKit;

namespace Wrapper
{
    public class FilterBookViewModel : BaseViewModel
    {
        private IUserLibraryManager UserLibraryManager { get; set; }

        // Commandes 
        public RelayCommand<string> LoadDataAuthor { get; private set; }
        public RelayCommand<string> LoadDataPublishDate { get; private set; }
        public RelayCommand<string> LoadDataNote { get; private set; }

        // ---------------------
        public ObservableCollection<ItemFilter> ItemFilters { get; private set; } = new ObservableCollection<ItemFilter>() ;


        public FilterBookViewModel(IUserLibraryManager userLibraryManager)
        {
            this.UserLibraryManager = userLibraryManager;

            LoadDataAuthor = new RelayCommand<string>(o => LoadDataAuthorFonction());
            LoadDataPublishDate = new RelayCommand<string>(o => LoadDataPublishDateFonction());
            LoadDataNote = new RelayCommand<string>(o => LoadDataNoteFonction());
        }

        
        // Load on author
        private void LoadDataAuthorFonction()
        {
            var result = UserLibraryManager.GetBooksFromCollection(0,((int)UserLibraryManager.GetBooksFromCollection(0, 0).Result.Item1));

            if ( result.Result.Item2.Count() == 0  ) { return; }
            ItemFilters.Clear();

            List<Author> authors = new List<Author>();
            int nbAuthorUnknown = 0;

            // Cherche tous les nom des auteurs 
            foreach (Book book in result.Result.Item2)
            {
                if ( book.Authors.Count() == 0)
                {
                    if (!authors.Contains(new Author() { Id = "Inconnu" }))
                    {
                        authors.Add(new Author() { Id = "Inconnu",Name = "Inconnu" });
                    }
                    nbAuthorUnknown++;
                } 
                else
                {
                    foreach (Author author in book.Authors)
                    {
                        if (!authors.Contains(author))
                        {
                            authors.Add(author);
                        }
                    }
                }
            }

            // Trier la liste
            authors = authors.OrderBy(author => author.Name).ToList();

            // Ajoute un nouvel item Filter a la liste 
            foreach(Author author in authors)
            {
                int nbBook = UserLibraryManager.GetNbBooksByAuthorId(author.Id);
                if (author.Id == "Inconnu")
                {
                    ItemFilters.Add(new ItemFilter() { DataFilter = "Inconnu", Type = new Tuple<TypeItemFilter, string>(TypeItemFilter.Author,"Inconnu"), NumberBook = nbAuthorUnknown.ToString() });
                }
                else
                {
                    ItemFilters.Add(new ItemFilter() { DataFilter = author.Name, Type = new Tuple<TypeItemFilter, string>(TypeItemFilter.Author,author.Name), NumberBook = nbBook.ToString() });
                }
            }           
        }



        private void LoadDataPublishDateFonction()
        {
            var result = UserLibraryManager.GetBooksFromCollection(0, ((int)UserLibraryManager.GetBooksFromCollection(0, 0).Result.Item1));

            if (result.Result.Item2.Count() == 0) { return; }
            ItemFilters.Clear();

            List<int> annees = new List<int>();

            // Cherche toute les dates 
            foreach (Book book in result.Result.Item2)
            {
                if (!annees.Contains(book.PublishDate.Year))
                {
                    annees.Add(book.PublishDate.Year);
                }
            }

            // Trier la liste
            annees.Sort();

            // Ajoute un nouvel item Filter a la liste 
            foreach (int annee in annees)
            {
                int nbBook = UserLibraryManager.GetNbBooksByYear(annee);
                ItemFilters.Add(new ItemFilter() { DataFilter = annee.ToString(), Type = new Tuple<TypeItemFilter, string>(TypeItemFilter.PublishDate,annee.ToString()), NumberBook = nbBook.ToString() });
            }
        }

        // Load on author
        private void LoadDataNoteFonction()
        {
            var result = UserLibraryManager.GetBooksFromCollection(0, ((int)UserLibraryManager.GetBooksFromCollection(0, 0).Result.Item1));

            if (result.Result.Item2.Count() == 0) { return; }
            ItemFilters.Clear();

            List<float> note = new List<float>();

            // Cherche toute les notes 
            foreach (Book book in result.Result.Item2)
            {
                if ( book.UserRating != null )
                {
                    if (!note.Contains(book.UserRating.Value))
                    {
                        note.Add(book.UserRating.Value);
                    }
                }
            }

            // Trier la liste
            note.Sort();

            // Ajoute un nouvel item Filter a la liste 
            foreach (float rating in note)
            {
                int nbBook = UserLibraryManager.GetNbBooksByRating(rating);
                ItemFilters.Add(new ItemFilter() { DataFilter = rating.ToString(), Type = new Tuple<TypeItemFilter, string>(TypeItemFilter.Note, rating.ToString()), NumberBook = nbBook.ToString() });
            }
        }
    }
}
