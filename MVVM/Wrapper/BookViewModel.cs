﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolKit;

namespace Wrapper
{
    public class BookViewModel : BaseViewModel
    {

        IUserLibraryManager UserLibraryManager;

        public RelayCommand<string> AddBookToBeRead { get; private set; }


        public string ImageLarge
        {
            get => imageLarge;
            set
            {
                setProperty(ref imageLarge,value);
            }
        }
        private string imageLarge;

        public string Title
        {
            get => title;
            set
            {
                setProperty(ref title,value);
            }
        }
        private string title;

        public string Authors
        {
            get => authors;
            set { setProperty(ref authors, value); }
        }
        public string authors;

        public string PublisherAndPublishDate
        {
            get => publisherDate;
            set { setProperty(ref publisherDate, value); }
        }
        private string publisherDate;

        public string Resume
        {
            //get => "Résumé trouver l'attribut";
            get => resume;
            set 
            { 
                setProperty(ref resume,value);
            }
        }
        private string resume;

        public string NbPages
        {
            get => nbPages;
            set 
            {
                setProperty(ref nbPages,value);
            }
        }
        private string nbPages;
        

        public string Language
        {
            get => language;
            set { setProperty(ref language, value); }
        }
        private string language;

        public string ISBN
        {
            get => isbn;
            set { setProperty(ref isbn, value); }
        }
        private string isbn;

        public string Statut
        {
            get => statut ;
            set { setProperty(ref statut, value); }
        }
        private string statut;

        public string AddLibrary
        {
            get => addLibrary;
            set { setProperty(ref addLibrary, value); }
        }
        private string addLibrary;

        private Book Book { get; set; }

        public BookViewModel(IUserLibraryManager userLibraryManager)
        {
            this.UserLibraryManager = userLibraryManager;

            AddBookToBeRead = new RelayCommand<string>(o => AddBookToBeReadFonction());
        }

        private void AddBookToBeReadFonction()
        {
            Book.Status = Status.ToBeRead;
            UserLibraryManager.UpdateBook(Book);
        }


        public bool LoadBook(string isbn13)
        {
            Book book = UserLibraryManager.GetBookByISBN(isbn13).Result;
            if (book == null) { return false; }

            Book = book;

            // Image 
            ImageLarge = Book.ImageLarge;
            // Authors 
            if (Book.Authors.Count() == 0)
            {
                Authors = "Inconnu";
            }
            else
            {
                string result = "";
                foreach (Author name in Book.Authors)
                {
                    result += name.Name;
                    result += ", ";
                }
                Authors = result;
            }
            // PublisherAndPublishDate
            string publisher = "";
            foreach (string name in Book.Publishers)
            {
                publisher += name + ", ";
            }
            PublisherAndPublishDate = publisher + " (" + Book.PublishDate.ToShortDateString() + ")";
            // Title
            Title = Book.Title;
            // Resume
            Resume = Book.UserNote;
            // NbPages
            NbPages = Book.NbPages.ToString();
            // Language
            Language = Book.Language.ToString();
            // ISBN
            ISBN = Book.ISBN13;
            // Statut
            Statut = Book.Status.ToString();
            // AddLibrary
            AddLibrary = "AddLibrary !!";

            return true;
        }
    }
}
