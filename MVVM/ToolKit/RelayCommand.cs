﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToolKit
{
    public class RelayCommand<T> : ICommand
    {
        public event EventHandler? CanExecuteChanged;

        private readonly Action<T> execute;
        private readonly Func<T, bool> canExecute;

        
        public RelayCommand(Action<T> execute,Func<T,bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        
        public bool CanExecute(object? parameter)
        {
            if (parameter == null)
                return true;
            return canExecute((T)parameter);
        }

        public void Execute(object? parameter = null)
        {
            execute((T) parameter);
        }


    }
}
