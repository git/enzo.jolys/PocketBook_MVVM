﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolKit
{
    public class WorkOnListBooks
    {

        public WorkOnListBooks() { }


        public List<GroupBooks> UpdateGroupBooks(List<Book> Books)
        {
            List<GroupBooks> GroupBooks = new List<GroupBooks>();
            foreach (Book book in Books)
            {
                if (book.Authors.Count != 0)
                {
                    foreach (Author author in book.Authors)
                    {
                        int find = -1;
                        for (int i = 0; i < GroupBooks.Count(); i++)
                        {
                            if (GroupBooks[i].Author == author.Name)
                            {
                                find = i;
                                break;
                            }
                        }
                        if (find != -1)
                        {
                            GroupBooks[find].Add(book);
                        }
                        else
                        {
                            GroupBooks.Add(new GroupBooks(author.Name, new List<Book> { book }));
                        }
                    }
                }
                else
                {
                    int index = -1;
                    for (int i = 0; i < GroupBooks.Count(); i++)
                    {
                        if (GroupBooks[i].Author == "Inconnu")
                        {
                            index = i;
                            break;
                        }
                    }
                    if (index == -1)
                    {
                        GroupBooks.Add(new GroupBooks("Inconnu", new List<Book> { book }));
                    }
                    else
                    {
                        GroupBooks[index].Add(book);
                    }
                }
            }
            return GroupBooks;
        }
    }
}
