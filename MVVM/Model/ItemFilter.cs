﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ItemFilter
    {

        public string DataFilter {  get; set; } 

        public string NumberBook { get; set; }

        public Tuple<TypeItemFilter,string> Type { get; set; }

    }
}
