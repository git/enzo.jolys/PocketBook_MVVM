﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class GroupBooks : List<Book>
    {
        public string Author { get; set; }

        public GroupBooks( string author, List<Book> ListBook) : base(ListBook)
        {
            Author = author;
        }
    }
}
